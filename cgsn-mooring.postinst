#!/bin/bash
set -e

if pidof systemd >/dev/null && ! ischroot; then
  /bin/systemctl daemon-reload
  /bin/systemctl enable \
    cgsn \
    cgsn-gobyd \
    cgsn-logger \
    cgsn-supervisor \
    cgsn-scheduler \
    cgsn-driver-port@{1..8} \
    cgsn-legacy-logger-port@{1..8} \
    cgsn-system-health
  
  . /usr/share/debconf/confmodule

  db_get cgsn-mooring/mooring_name
  MOORING_NAME=$RET

  db_get cgsn-mooring/cpu_name
  CPU_NAME=$RET
  OLD_HOST_NAME=`/bin/hostname`
  NEW_HOST_NAME=${MOORING_NAME}-${CPU_NAME}
  
  if [[ "$NEW_HOST_NAME" != "$OLD_HOST_NAME" ]]; then
      echo "Setting hostname to $NEW_HOST_NAME (log out and in again for the prompt to change)"

      /usr/bin/hostnamectl set-hostname $NEW_HOST_NAME
      sed -i "s/^\(127\.0\.[0-9\.]*\).*${OLD_HOST_NAME}$/\1\t${NEW_HOST_NAME}/" /etc/hosts
      /bin/systemctl restart networking
  fi

  echo "Starting cgsn software..."
  /bin/systemctl restart cgsn
fi


# write the mooring and CPU name to an environment file for systemd to use
MOORING_NAME=$(/usr/bin/cgsn_config_tool --action WRITE_MOORING_NAME)
CPU_NAME=$(/usr/bin/cgsn_config_tool --action WRITE_CPU_NAME)
cat <<EOF > /etc/cgsn-mooring/id
CGSN_MOORING_NAME=${MOORING_NAME}
CGSN_CPU_NAME=${CPU_NAME}
EOF

#DEBHELPER#

exit 0
