# CGSN Mooring Debian packaging

This codebase `cgsn-mooring-debian` is the Debian source packaging for [cgsn-mooring](https://bitbucket.org/ooicgsn/cgsn-mooring).

This repository is used to create a non-native Debian package (i.e. the Debian packaging is not included in the upstream source code). See [this article](https://wiki.debian.org/DebianMentorsFaq#What_is_the_difference_between_a_native_Debian_package_and_a_non-native_package.3F) for more of the rationale behind this choice.

For details on the meaning and use of these files, please start with the [Debian New Maintainers' Guide](https://www.debian.org/doc/manuals/maint-guide/)

The documentation about use of this package is maintained here: [https://bitbucket.org/ooicgsn/cgsn-mooring/src/master/src/doc/markdown/doc_11-packaging.md](https://bitbucket.org/ooicgsn/cgsn-mooring/src/master/src/doc/markdown/doc_11-packaging.md).
